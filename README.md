## Django Development With Docker Compose

### Dependencies:

- Docker v1.10.3
- Docker Compose v1.6.2
- Python 3.5


### Instructions

1. Build images - `docker-compose build`
1. Start services - `docker-compose up -d`
1. Create migrations - `docker-compose run web /usr/local/bin/python manage.py migrate`
